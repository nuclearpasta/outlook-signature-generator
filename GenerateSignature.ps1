# RR: 10th MAR 2019
# This script will generate signature based on a predefiend format
# RR: 31 MAR 2019
# fixed the spelling for bogura and chattogram

$UserName = $env:username
$Filter = "(&(objectCategory=User)(samAccountName=$UserName))"
$Searcher = New-Object System.DirectoryServices.DirectorySearcher
$Searcher.Filter = $Filter
$ADUserPath = $Searcher.FindOne()
$ADUser = $ADUserPath.GetDirectoryEntry()
$ADDisplayName = $ADUser.givenName.ToString().trim() + " " + $ADUser.sn.ToString().trim()
$ADEmailAddress = $ADUser.mail
$ADTitle = $ADUser.title
$ADDescription = $ADUser.description
$ADTelePhoneNumber = $ADUser.TelephoneNumber
$ADFax = $ADUser.facsimileTelephoneNumber
$ADMobile = $ADUser.mobile
$ADStreetAddress = $ADUser.streetaddress
$ADModify = $ADUser.whenChanged
$ADOffice = $ADUser.physicalDeliveryOfficeName
$rawFile = ''
if($ADOffice -eq 'CHO Extension')
{
    $rawFile = 'raw_mehnaz.txt'
}
elseif ($ADOffice -eq 'Corporate Head Office')
{
    $rawFile = 'raw_silver.txt'
}
elseif ($ADOffice -eq 'Bogura')
{
    $rawFile = 'raw_bogura.txt'
}
elseif ($ADOffice -eq 'Gazipur')
{
    $rawFile = 'raw_gazipur.txt'
}
elseif ($ADOffice -eq 'Chattogram')
{
    $rawFile = 'raw_chattogram.txt'
}
else
{
    exit;
}
$rawFile = '\\192.168.1.10\DeployAD\signature\' + $rawFile
#$src = (Get-Content -Path .\raw.txt) 
$src = (Get-Content -Path $rawFile) 
Remove-ItemProperty -Name 'NewSignature' -Path HKCU:'\Software\Microsoft\Office\15.0\Common\MailSettings' -ErrorAction SilentlyContinue
Remove-ItemProperty -Name 'ReplySignature' -Path HKCU:'\Software\Microsoft\Office\15.0\Common\MailSettings' -ErrorAction SilentlyContinue

$AppData=(Get-Item env:appdata).value
#$SigPath = '\Microsoft\Signatures'
#$LocalSignaturePath = $AppData+$SigPath
$SigFile = $AppData + '\Microsoft\Signatures\Signature.htm' 

$mobileview = $ADUser.mobile -replace '-',' '
$mobileview = $mobileview.Substring(1)
$ADUser.mail = $ADUser.mail -ceq $ADUser.mail.ToLower();
$src = $src -replace '%%username%%', $ADDisplayName
$src = $src -replace '%%title%%', $ADUser.Title
$src = $src -replace '%%department%%', $ADUser.department
$src = $src -replace '%%email%%', $ADUser.mail 
$src = $src -replace '%%mobile%%', $ADUser.mobile
$src = $src -replace '%%mobileview%%', $mobileview

$src | Out-File -FilePath $SigFile

Write-Output "Setting signature for Office 2013 as forced"
If (Get-ItemProperty -Name 'NewSignature' -Path HKCU:'\Software\Microsoft\Office\15.0\Common\MailSettings' -ErrorAction SilentlyContinue) { } 
Else { 
    New-ItemProperty HKCU:'\Software\Microsoft\Office\15.0\Common\MailSettings' -Name 'NewSignature' -Value 'Signature' -PropertyType 'String' -Force 
    } 
    If (Get-ItemProperty -Name 'ReplySignature' -Path HKCU:'\Software\Microsoft\Office\15.0\Common\MailSettings' -ErrorAction SilentlyContinue) { } 
    Else { 
    New-ItemProperty HKCU:'\Software\Microsoft\Office\15.0\Common\MailSettings' -Name 'ReplySignature' -Value 'Signature' -PropertyType 'String' -Force
    } 
